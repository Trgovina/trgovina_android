package com.trgovina.com.main.view_type_activity;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.trgovina.com.R;
import com.trgovina.com.adapter.view_type_adapter.AddDetailAdap;
import com.trgovina.com.fcm_push_notification.Config;
import com.trgovina.com.fcm_push_notification.NotificationMessageDialog;
import com.trgovina.com.fcm_push_notification.NotificationUtils;
import com.trgovina.com.main.activity.FilterData2D;
import com.trgovina.com.pojo_class.product_category.FilterData;
import com.trgovina.com.pojo_class.product_category.FilterKeyValue;
import com.trgovina.com.utility.BaseActivity;
import com.trgovina.com.utility.ClickListener;
import com.trgovina.com.utility.CommonClass;
import com.trgovina.com.utility.VariableConstants;

import java.util.ArrayList;

public class AddDetailActivity extends BaseActivity implements ClickListener,View.OnClickListener{

    private NotificationMessageDialog mNotificationMessageDialog;
    private RecyclerView rV_fieldList;
    private ArrayList<FilterData> filterData;
    private AddDetailAdap addDetailAdap;
    private Activity mActivity;
    private RelativeLayout rL_done;
    private RelativeLayout root_addDetail;
    private ArrayList<FilterKeyValue> filterKeyValues;
    private String title;
    private TextView tV_title;
    private String categoryNodeId="",subCategoryNodeId="", id = "";

    // isForFilter used while view type is open from filter screen
    private boolean isForFilter;

    private ArrayList<FilterKeyValue> selectedFileds;
//    private ArrayList<FilterData> selectedDataFields;
//    private ArrayList<FilterData> filterDataKeyValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_detail);

        mActivity=AddDetailActivity.this;
        mNotificationMessageDialog=new NotificationMessageDialog(mActivity);

        filterKeyValues=new ArrayList<>();
        //selectedDataFields = new ArrayList<>();
        //filterDataKeyValue=new ArrayList<>();

        filterData = (ArrayList<FilterData>) getIntent().getSerializableExtra("filterData");
        //selectedDataFields = (ArrayList<FilterData>) getIntent().getSerializableExtra("selectedDataField");
        title = getIntent().getStringExtra("title");
        isForFilter=getIntent().getBooleanExtra("isForFilter",false);
        categoryNodeId=getIntent().getStringExtra("categoryNodeId");
        subCategoryNodeId=getIntent().getStringExtra("subCategoryNodeId");
        Log.d(mActivity.toString(), "filter data = " + categoryNodeId + "   " + subCategoryNodeId + " " + id);
        if(getIntent()!=null) {
            selectedFileds = (ArrayList<FilterKeyValue>) getIntent().getSerializableExtra("selectedField");
           // selectedDataFields = (ArrayList<FilterData>) getIntent().getSerializableExtra("selectedDataField");
            if(selectedFileds!=null && selectedFileds.size()>0) {
                filterKeyValues = selectedFileds;

            }
           /* if(selectedDataFields!=null && selectedDataFields.size()>0) {
                filterDataKeyValue = selectedDataFields;

            }*/
        }



        //set again all values which is filled by user
        if(selectedFileds!=null && selectedFileds.size()>0){
            for(int i=0;i<filterData.size();i++){
                for(FilterKeyValue f:selectedFileds){
                    if(f.getKey().equalsIgnoreCase(filterData.get(i).getFieldName())){
                        filterData.get(i).setSelected(true);
                        filterData.get(i).setSelectedValues(f.getValue());
                    }
                }
            }
        }

        tV_title=(TextView)findViewById(R.id.tV_title);
        tV_title.setText(title);

        rV_fieldList = (RecyclerView)findViewById(R.id.rV_fieldList);
        rV_fieldList.setLayoutManager(new LinearLayoutManager(this));
        addDetailAdap=new AddDetailAdap(mActivity,filterData);
        rV_fieldList.setAdapter(addDetailAdap);

        rL_done = (RelativeLayout)findViewById(R.id.rL_done);
        rL_done.setOnClickListener(this);
        root_addDetail = (RelativeLayout)findViewById(R.id.root_addDetail);

        RelativeLayout rL_back_btn=(RelativeLayout)findViewById(R.id.rL_back_btn);
        rL_back_btn.setOnClickListener(this);
    }

    /*
     * 1 : textbox
     * 2 : checkbox
     * 3 : slider
     * 4 : radio button
     * 5 : range
     * 6 : drop down
     * 7 : date
     */

    @Override
    public void onItemClick(View view, int position) {
        FilterData filter=filterData.get(position);
        Intent intent;
        Log.d("AddDetailActivity  code " , "" + filter.getType());
        switch (filter.getType()){
            case 1:
                intent = new Intent(mActivity,EditTextTypeActivity.class);
                intent.putExtra("title",filter.getFieldName());
                intent.putExtra("fieldName",filter.getFieldName());
                intent.putExtra("selectedValue",filter.getSelectedValues());
                intent.putExtra("position",position);
                intent.putExtra("isNumber","0");
                intent.putExtra("categoryNodeId",categoryNodeId);
                intent.putExtra("subCategoryNodeId",subCategoryNodeId);
                startActivityForResult(intent,VariableConstants.SELECT_VALUE_REQ_CODE);
                break;
            case 2:
                intent = new Intent(mActivity, MultipleSelectionActivity.class);
                intent.putExtra("values",filter.getValues());
                intent.putExtra("title",filter.getFieldName());
                intent.putExtra("fieldName",filter.getFieldName());
                intent.putExtra("position",position);
                intent.putExtra("categoryNodeId",categoryNodeId);
                intent.putExtra("subCategoryNodeId",subCategoryNodeId);
                startActivityForResult(intent,VariableConstants.SELECT_VALUE_REQ_CODE);
                break;
            case 3:
                intent = new Intent(mActivity, SeekBarViewActivity.class);
                intent.putExtra("values",filter.getValues());
                intent.putExtra("title",filter.getFieldName());
                intent.putExtra("fieldName",filter.getFieldName());
                intent.putExtra("position",position);
                intent.putExtra("categoryNodeId",categoryNodeId);
                intent.putExtra("subCategoryNodeId",subCategoryNodeId);
                startActivityForResult(intent,VariableConstants.SELECT_VALUE_REQ_CODE);
                break;
            case 4:
                intent = new Intent(mActivity, SingleSelectionActivity.class);
                intent.putExtra("selectedValue",filter.getSelectedValues());
                intent.putExtra("values",filter.getValues());
                intent.putExtra("title",filter.getFieldName());
                intent.putExtra("fieldName",filter.getFieldName());
                intent.putExtra("position",position);
                intent.putExtra("categoryNodeId",categoryNodeId);
                intent.putExtra("subCategoryNodeId",subCategoryNodeId);
                startActivityForResult(intent,VariableConstants.SELECT_VALUE_REQ_CODE);
                break;
            case 5:
                intent = new Intent(mActivity,EditTextTypeActivity.class);
                intent.putExtra("title",filter.getFieldName());
                intent.putExtra("fieldName",filter.getFieldName());
                intent.putExtra("selectedValue",filter.getSelectedValues());
                intent.putExtra("position",position);
                intent.putExtra("isNumber","1");
                intent.putExtra("categoryNodeId",categoryNodeId);
                intent.putExtra("subCategoryNodeId",subCategoryNodeId);
                startActivityForResult(intent,VariableConstants.SELECT_VALUE_REQ_CODE);
                break;
            case 6:
                intent = new Intent(mActivity, SingleSelectionActivity.class);
                intent.putExtra("selectedValue",filter.getSelectedValues());
                intent.putExtra("values",filter.getValues());
                intent.putExtra("title",filter.getFieldName());
                intent.putExtra("fieldName",filter.getFieldName());
                intent.putExtra("position",position);
                intent.putExtra("categoryNodeId",categoryNodeId);
                intent.putExtra("subCategoryNodeId",subCategoryNodeId);
                startActivityForResult(intent,VariableConstants.SELECT_VALUE_REQ_CODE);
                startActivity(intent);
                break;
            case 7:
                intent = new Intent(mActivity,DatePickerActivity.class);
                intent.putExtra("title",filter.getFieldName());
                intent.putExtra("fieldName",filter.getFieldName());
                intent.putExtra("selectedValue",filter.getSelectedValues());
                intent.putExtra("position",position);
                intent.putExtra("isNumber","0");
                intent.putExtra("categoryNodeId",categoryNodeId);
                intent.putExtra("subCategoryNodeId",subCategoryNodeId);
                startActivityForResult(intent,VariableConstants.SELECT_VALUE_REQ_CODE);
                break;
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.rL_back_btn:
                onBackPressed();
                break;
            case R.id.rL_done:
                if(isMendatoryFill()){
                    Intent intent=new Intent();
                    intent.putExtra("filterKeyValues",filterKeyValues);
                    intent.putExtra("categoryNodeId",categoryNodeId);
                    intent.putExtra("subCategoryNodeId",subCategoryNodeId);
                   // intent.putExtra("selectedDataField",filterDataKeyValue);
                    Log.d("exe","addadetail"+"categoryNodeId"+categoryNodeId+"subCategoryNodeId"+subCategoryNodeId);
                    setResult(VariableConstants.ADD_DETAIL_DATA_REQ_CODE,intent);
                    onBackPressed();
                }
                break;
        }
    }

    public boolean isMendatoryFill(){
        for(FilterData fData:filterData){
            if(fData.isMandatory()){
                if(!fData.isSelected()) {
                    CommonClass.showSnackbarMessage(root_addDetail,fData.getFieldName()+" is not selected.");
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(data!=null) {
            switch (resultCode) {
                case VariableConstants.SELECT_VALUE_REQ_CODE:



                    String value = data.getStringExtra("selectedValue");
                    int pos = data.getIntExtra("position", -1);

                    String fieldName = data.getStringExtra("filedName");
                    String id = data.getStringExtra("id");

                    filterData.get(pos).setSelected(true);
                    filterData.get(pos).setSelectedValues(value);

//                    filterData.get(pos).setId("123");
//                    filterData.get(pos).setFieldName(fieldName);
//                    filterData.get(pos).setValues(value);

//                    selectedDataFields.get(pos).setId("123");
//                    selectedDataFields.get(pos).setFieldName(fieldName);
//                    selectedDataFields.get(pos).setValues(value);
                    //selectedDataFields.add(new FilterData2D("123", fieldName, value));

                    if(filterData.get(pos).getType()==5 || filterData.get(pos).getType()==3) {
                        // user change the value then first remove and add.
                        for(int i=0;i<filterKeyValues.size();i++) {
                            if(filterKeyValues.get(i).getKey().equalsIgnoreCase(filterData.get(pos).getFieldName()))
                                filterKeyValues.remove(i);
                        }
                        filterKeyValues.add(new FilterKeyValue("1", filterData.get(pos).getId(), value));
                    }
                    else {
                        // user change the value then first remove and add.
                        for(int i=0;i<filterKeyValues.size();i++) {
                            if (filterKeyValues.get(i).getKey().equalsIgnoreCase(filterData.get(pos).getFieldName()))
                                filterKeyValues.remove(i);
                        }
                        filterKeyValues.add(new FilterKeyValue(filterData.get(pos).getId(), value));
                    }

//                    if(filterData.get(pos).getType()==5 || filterData.get(pos).getType()==3) {
//                        // user change the value then first remove and add.
//                        for(int i=0;i<filterDataKeyValue.size();i++) {
////                            if(filterDataKeyValue.get(i).getKey().equalsIgnoreCase(filterData.get(pos).getFieldName()))
////                                filterDataKeyValue.remove(i);
//                        }
//                        filterDataKeyValue.add(new FilterData(selectedDataFields.get(pos).getId(), filterData.get(pos).getFieldName(), value));
//                    }
//                    else {
//                        // user change the value then first remove and add.
//                        for(int i=0;i<filterDataKeyValue.size();i++) {
////                            if (filterDataKeyValue.get(i).getKey().equalsIgnoreCase(filterData.get(pos).getFieldName()))
////                                filterDataKeyValue.remove(i);
//                        }
//                        filterDataKeyValue.add(new FilterData(selectedDataFields.get(pos).getId(),filterData.get(pos).getFieldName(), value));
//                    }

                    //filterDataKeyValue.add(new FilterData(selectedDataFields.get(pos).getId(),filterData.get(pos).getFieldName(), value));

                    //System.out.println( "" + selectedDataFields.get(pos).getId() + "  " + filterData.get(pos).getFieldName() + " " + value);

                    addDetailAdap.notifyItemChanged(pos);
                    break;
            }
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog.mRegistrationBroadcastReceiver, new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog.mRegistrationBroadcastReceiver, new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog.mRegistrationBroadcastReceiver);
        super.onPause();
    }
}
