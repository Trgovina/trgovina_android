package com.trgovina.com.main.activity.products;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import com.trgovina.com.R;
import com.trgovina.com.main.activity.SplashActivity;
import com.trgovina.com.utility.BaseActivity;
import com.trgovina.com.utility.LocaleHelper;
import com.trgovina.com.utility.SessionManager;

public class LanguageSelectActivity extends BaseActivity implements RadioGroup.OnCheckedChangeListener {
    private SessionManager mSessionManager;
    private Activity mActivity;
    private RadioButton rb_sb, rb_mg,rb_en;
    private RadioGroup rg_language;
    private String language_code;
    private int user_language_type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language_select);
        mActivity = LanguageSelectActivity.this;

        mSessionManager = new SessionManager(mActivity);

        // radio button for language
        rb_sb = (RadioButton) findViewById(R.id.rb_sb);
        rb_mg = (RadioButton) findViewById(R.id.rb_mg);
        rb_en = (RadioButton) findViewById(R.id.rb_en);

        rg_language = (RadioGroup) findViewById(R.id.rg_language);

        // intial checked button for current language
        currentLanguage(mSessionManager.getUserLanguageType());

        rg_language.setOnCheckedChangeListener(this);

        RelativeLayout rL_back_btn = (RelativeLayout) findViewById(R.id.rL_back_btn);

        rL_back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();

            }
        });

        RelativeLayout rL_done = (RelativeLayout) findViewById(R.id.rL_apply);

        rL_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mSessionManager.setLanguageCode(language_code);
                LocaleHelper.setLocale(getApplicationContext(),language_code);
                mSessionManager.seUsertLanguageType(user_language_type);

                Intent i = new Intent(mActivity, SplashActivity.class);
                // set the new task and clear flags
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);

            }

        });

    }

    public void currentLanguage(int language_type) {

        switch (language_type) {

            case 0:
                rb_sb.setChecked(true);
                user_language_type=0;
                break;

            case 1:
                rb_mg.setChecked(true);
                user_language_type=1;
                 break;

            case 2:
                rb_en.setChecked(true);
                user_language_type=2;
                break;

            default:
                rb_sb.setChecked(true);


        }

    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {

        switch (checkedId) {

            case R.id.rb_sb:
                language_code = getString(R.string.default_language_code);
                user_language_type=0;
                break;

            case R.id.rb_mg:
                language_code = getString(R.string.montenegrian_language_code);
                user_language_type=1;
                 break;

            case R.id.rb_en:
                language_code = getString(R.string.english_language_code);
                user_language_type=2;
                break;

        }

    }

}
